import wave
import sounddevice
import struct
import numpy

channels = 1
sample_rate = 44100
dtype = 'float32'


def record_wave(duration=1):
    frames = int(duration * sample_rate)
    numpy_array = sounddevice.rec(frames, sample_rate, channels, dtype, blocking=True)
    array = []
    for value in numpy_array:
        int_value = int(value[0] * 32767);
        array.append(int_value)
    return array


def play_wave(array):
    numpy_array = numpy.empty((len(array), 1), dtype=numpy.float32)
    for i in range(0, len(array)):
        float_value = float(array[i]) / 32767
        numpy_array[i][0] = float_value
    sounddevice.play(numpy_array, blocking=True);


def save_wave(filename, array):
    file = wave.open(filename, 'wb')
    file.setparams((channels, 2, sample_rate, 0, 'NONE', 'not compressed'))
    for value in array:
        # pack short
        packed_value = struct.pack('h', value)
        file.writeframes(packed_value)
    file.close()


def read_wave(filename):
    file = wave.open(filename, 'rb')
    frames = file.getnframes()
    array = []
    for i in range(0, frames):
        value = file.readframes(1)
        # unpack short
        unpacked_value = struct.unpack('h', value)[0]
        array.append(unpacked_value)
    return array