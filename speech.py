# This Python file uses the following encoding: utf-8

import os
import audio
import plotter

alphabet_waves_dir_name = 'waves'
alphabet_waves_by_symbol = []
alphabet_symbols_rus_rus = ['а', 'б', 'в']#, 'г', 'д', 'е', 'ё', 'ж', 'з', 'и',
                            #'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у',
                            #'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ы', 'э', 'ю', 'я']

alphabet_symbols_rus_eng = ['a', 'b', 'v']#, 'g', 'd', 'е', 'io', 'j', 'z', 'i',
                            #'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'y',
                            #'f', 'h', 'c', 'ch', 'sh', 'she', 'ie', 'iu', 'ia']


def init():
    if not os.path.exists(alphabet_waves_dir_name):
        os.mkdir(alphabet_waves_dir_name)
    os.chdir(alphabet_waves_dir_name)
    for symbol in alphabet_symbols_rus_eng:
        if not os.path.exists(symbol):
            print('u need say ' + symbol)
            wave = audio.record_wave(1)
            audio.save_wave(symbol, wave)
        else:
            wave = audio.read_wave(symbol)
        alphabet_waves_by_symbol.append(wave)
    os.chdir('..')


def synthesis(text):
    array = []
    for symbol in text:
        index = alphabet_symbols_rus_eng.index(symbol)
        array += alphabet_waves_by_symbol[index]
    audio.play_wave(array)
    plotter.plot_array(array)
