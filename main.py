import speech
import audio
import plotter
import numpy
import numpy.fft as fft
import matplotlib.pyplot as pyplot

if __name__ == '__main__':
    speech.init()
    print('I\'am ready')
    while True:
        commandline = input()
        tokens = commandline.split(' ')
        command = tokens[0]
        if command == 'exit':
            break
        elif command == 'init':
            speech.init()
        elif command == 'fft' and len(tokens) == 2:
            wav = audio.read_wave(tokens[1])
            numpy_wav = numpy.array(wav)
            sp = fft.rfft(numpy_wav)
            pyplot.plot(sp.real, sp.imag)
            pyplot.show()
        elif command == 'speak' and len(tokens) == 2:
            speech.synthesis(tokens[1])
        elif command == 'play' and len(tokens) == 2:
            wav = audio.read_wave(tokens[1])
            audio.play_wave(wav)
        elif command == 'plot' and len(tokens) == 2:
            wav = audio.read_wave(tokens[1])
            plotter.plot_array(wav)
        elif command == 'crop' and len(tokens) == 4:
            wav = audio.read_wave(tokens[1])
            l = int(tokens[2])
            r = int(tokens[3])
            audio.save_wave(tokens[1], wav[l:r])
        elif command == 'rec' and len(tokens) == 3:
            duration = int(tokens[2])
            wav = audio.record_wave(duration)
            audio.save_wave(tokens[1], wav)
        else:
            print('what?')
