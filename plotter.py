import matplotlib.pyplot as plotter


def plot_array(array, label=''):
    plotter.plot(array)
    plotter.xlabel(label)
    plotter.show()
